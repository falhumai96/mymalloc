#ifndef MYMALLOC_H
#define MYMALLOC_H

#include <stddef.h>
#include <stdbool.h>


#define MEMSPACE_SIZE 420000000

/* Structs. */
typedef struct _block_info {
    char* ref;
    size_t size;
} block_info;

/* Public functions. */
void* my_malloc(size_t size);
void my_free(void* ptr);
void* my_calloc(size_t nmemb, size_t size);
void* my_realloc(void* ptr, size_t size);

/* Private functions. */
void* _allocate(size_t size);
int _is_space_free(size_t start_idx, size_t end_idx, char* current_ref);
size_t _get_ref_idx(char* ref);
char* _get_next_free_space(size_t size);
block_info* _get_existing_block(char* ptr);
void _add_block_info(char* ptr, size_t size);
void _remove_block_info(char* ptr);

#endif /* MYMALLOC_H */

