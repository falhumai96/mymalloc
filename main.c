#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mymalloc.h"

int main(void) {
    /* Some sample test code */
    char* temp_str = my_malloc(sizeof (char) * 90);
    strcpy(temp_str, "Hello, world!!\n");
    temp_str = my_realloc(temp_str, sizeof(char) * (strlen(temp_str) + 1));
    int* temp_int_arr = my_malloc(sizeof (int) * 900);
    int i;
    int size_test = 100;
    for (i = 0; i < size_test; i++) {
        temp_int_arr[i] = size_test - i - 1;
    }
    printf("%s\n", temp_str);
    for (i = 0; i < size_test; i++) {
        printf("%d\n", temp_int_arr[i]);
    }
    my_free(temp_str);
    my_free(temp_int_arr);
}

