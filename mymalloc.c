#include "mymalloc.h"
#include <stdlib.h>

char free_mem[MEMSPACE_SIZE];
block_info free_mem_info[MEMSPACE_SIZE];
size_t current_free_mem_info_size = 0;

/* Public functions. */
void* my_malloc(size_t size) {
    return _allocate(size);
}

void my_free(void* ptr) {
    _remove_block_info(ptr);
}

void* my_calloc(size_t nmemb, size_t size) {
    size_t full_size = nmemb * size;
    char* new_ref = _allocate(full_size);
    if (new_ref == NULL) {
        return NULL;
    }

    size_t start_idx = _get_ref_idx(new_ref);
    size_t end_idx = start_idx + full_size - 1;
    size_t i;
    for (i = start_idx; i < end_idx; i++) {
        free_mem[i] = 0;
    }
    return new_ref;
}

void* my_realloc(void* ptr, size_t size) {
    if (size <= 0) {
        return NULL;
    }
    if (_get_existing_block(ptr) == NULL) {
        return _allocate(size);
    }
    size_t ptr_start_idx = _get_ref_idx(ptr);
    size_t ptr_end_idx = ptr_start_idx + size - 1;
    int available_space_ret_code = _is_space_free(ptr_start_idx, ptr_end_idx, ptr);
    if (available_space_ret_code == 0) {
        _add_block_info(ptr, size);
        return ptr;
    }
    void* new_ref = _allocate(size);
    if (new_ref == NULL) {
        return NULL;
    }
    _remove_block_info(ptr);
    return new_ref;
}

/* Private functions. */
void* _allocate(size_t size) {
    char* new_ref = _get_next_free_space(size);
    if (new_ref == NULL) {
        return NULL;
    }
    _add_block_info(new_ref, size);
    return new_ref;
}

int _is_space_free(size_t start_idx, size_t end_idx, char* current_ref) {
    if (end_idx >= MEMSPACE_SIZE) {
        return 2;
    }

    size_t i;
    for (i = 0; i < current_free_mem_info_size; i++) {
        if (free_mem_info[i].ref == current_ref) {
            continue;
        }
        size_t current_lookup_ref_idx_start_idx =
                _get_ref_idx(free_mem_info[i].ref);
        size_t current_lookup_ref_idx_end_idx =
                current_lookup_ref_idx_start_idx +
                free_mem_info[i].size - 1;
        if ((current_lookup_ref_idx_start_idx >= start_idx &&
                current_lookup_ref_idx_start_idx <= end_idx) ||
                (current_lookup_ref_idx_end_idx >= start_idx &&
                current_lookup_ref_idx_end_idx <= end_idx)) {
            return 1;
        }
    }
    return 0;
}

size_t _get_ref_idx(char* ref) {
    return (MEMSPACE_SIZE -
            ((&free_mem[MEMSPACE_SIZE - 1] - ref) + 1));
}

char* _get_next_free_space(size_t size) {
    size_t i;
    for (i = 0; i < MEMSPACE_SIZE; i++) {
        size_t start_idx = i;
        size_t end_idx = i + size - 1;
        int free_space_ret_code = _is_space_free(start_idx, end_idx, NULL);
        if (free_space_ret_code == 0) {
            return &free_mem[start_idx];
        } else if (free_space_ret_code == 2) {
            return NULL;
        }
    }
    return NULL;
}

block_info* _get_existing_block(char* ptr) {
    size_t i;
    for (i = 0; i < current_free_mem_info_size; i++) {
        if (free_mem_info[i].ref == ptr) {
            return &free_mem_info[i];
        }
    }
    return NULL;
}

void _add_block_info(char* ptr, size_t size) {
    block_info* current_block = _get_existing_block(ptr);
    if (current_block != NULL) {
        current_block->size = size;
        return;
    }
    block_info new_block_info;
    new_block_info.ref = ptr;
    new_block_info.size = size;
    free_mem_info[current_free_mem_info_size] = new_block_info;
    current_free_mem_info_size++;
}

void _remove_block_info(char* ptr) {
    size_t to_remove_idx = -1;
    size_t i;
    for (i = 0; i < current_free_mem_info_size; i++) {
        if (free_mem_info[i].ref == ptr) {
            to_remove_idx = i;
        }
    }
    if (to_remove_idx == -1) {
        return;
    }
    for (i = to_remove_idx; i < current_free_mem_info_size - 1; i++) {
        free_mem_info[i] = free_mem_info[i + 1];
    }
    current_free_mem_info_size--;
}